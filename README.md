## qssi-user 11 RKQ1.211001.001 V13.0.9.0.RGCMIXM release-keys
- Manufacturer: xiaomi
- Platform: bengal
- Codename: spes
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.211001.001
- Incremental: V13.0.9.0.RGCMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/spes/spes:11/RKQ1.211001.001/V13.0.9.0.RGCMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.211001.001-V13.0.9.0.RGCMIXM-release-keys-random-text-24347162929305
- Repo: redmi_spes_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
